% �rbol geneal�gico

% primero se definen los parentescos b�sicos
% padre(A, B).

padre('Sergio', 'Martin').
padre('Sergio', 'Felipe').
padre('Juan', 'Eros').
padre('Juan', 'Cristobal').
padre('Cristina', 'Cristobal').
padre('Francisca', 'Martin').
padre('Francisca', 'Felipe').
padre('Jos�', 'Sergio').
padre('Cecilia', 'Sergio').
padre('Cecilia', 'Juan').
padre('Luis', 'Juan').


mujer('Cristina').
mujer('Francisca').
mujer('Cecilia').

% Predicado para saber si es madre
madre(A, B) :-
	padre(A, B), mujer(A).

% Se define el predicado para saber el hijo
hijo(+A, +B) :-
	padre(-B, -A).

% Ahora se definen los predicados para que dos personas
% sean hermanos.
hermanos(A, B) :-
	padre(X, A), padre(X, B), \==(A, B).

% Parentesco abuelo-nieto
nieto(A, B) :-
	hijo(A, X), hijo(X, B).

% Ahora se define predicad para saber si son primos
primos(A, B) :-
	padre(X, A), padre(Y, B), hermanos(X, Y).

% Definici�n del predicado t�o
tio(A, B) :-
	hermanos(A, X), padre(X, B).
