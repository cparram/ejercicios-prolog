% PROBANDO LA RECURSIVIDAD

% la longitud de una lista vaci~a es cero
longitud([], 0).

% recursividad no de cola
longitud([_| Resto], Longitud) :-
	longitud(Resto, Longitud_Resto),
	Longitud is Longitud_Resto + 1.

% tener en cuenta que, por ejemplo:
%		X is 3 + 2 => x = 5
%		X = 3 + 2  => x = 3 + 2 (igualdad simbo~lica)

% longitud de listas con recursividad de cola

longitud_cola_aux([], Parcial, Parcial).
longitud_cola_aux([_|Resto], Parcial, Resultado):-
	Longitud is Parcial + 1,
	longitud_cola_aux(Resto, Longitud, Resultado).

longitud_cola(Lista, Longitud):-
	longitud_cola_aux(Lista, 0, Longitud).



% Obtener el n�mero de fibonacci
fibonacci_aux(Penultimo, Ultimo, Iter, Indice, Number):-
	=(Iter, Indice), Number is +(Penultimo, Ultimo),!;
	SiguienteIter is Iter + 1,
	SiguienteUltimo is Penultimo + Ultimo,
	fibonacci_aux(Ultimo, SiguienteUltimo, SiguienteIter, Indice, Number).

fibonacci(Indice, Number):-
	=(Indice, 0), Number is 1, !;
	=(Indice, 1), Number is 1, !;
	fibonacci_aux(1, 1, 2, Indice, Number).

% Elemento a cierta posici�n
elemento_a_aux(Pos, [Cabeza | Resto], Indice, Elemento):-
	=(Pos, Indice), Elemento is Cabeza, !;
	is(Siguiente, +(Indice, 1)),
	elemento_a_aux(Pos, Resto, Siguiente, Elemento).

elemento_a(Pos, Lista, Elemento):-
	elemento_a_aux(Pos, Lista, 0, Elemento).

% Cl�usula que invierte una lista
reversa_aux([], Lista, Lista).
reversa_aux([C | R], ListaAux, ListaFinal):-
	reversa_aux(R, [C | ListaAux], ListaFinal).

reversa(Lista, Resultado) :-
	reversa_aux(Lista, [], Resultado).


es_palindromo(Lista):-
	reversa(Lista, Lista).

% Llevar todos los elementos de una lista al mismo plano
