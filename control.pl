
% Crear un predicado que agrupe los elementos de una lista
% Ejemplo:
%   predicado([a, a, b, b, c, c], X) => X = [[a, a], [b, b], [c ,c]]

% Cl�usula que invierte una lista
reversa_aux([], Lista, Lista).
reversa_aux([C | R], ListaAux, ListaFinal):-
	reversa_aux(R, [C | ListaAux], ListaFinal).

reversa(Lista, Resultado) :-
	reversa_aux(Lista, [], Resultado). 

% True si el Elemento pertenence a la lista.
pertenece(Elemento, [C | R]) :-
	=(Elemento, C), !;
	pertenece(Elemento, R).

% Primera soluci�n propuesta:
% Los agrupa pero no en el mismo orden.
sol1_aux([], UltimoGrupo, ListaAux, ListaFinal):-
	 ListaFinal = [UltimoGrupo | ListaAux].

sol1_aux([C | R], Grupo, ListaAux, ListaFinal) :-
	pertenece(C, Grupo),
	SiguienteGrupo = [C | Grupo],
	sol1_aux(R, SiguienteGrupo, ListaAux, ListaFinal), !;
	SiguienteListaAux = [Grupo | ListaAux],
	SiguienteGrupo = [C | []],
	sol1_aux(R, SiguienteGrupo, SiguienteListaAux, ListaFinal).

sol1([C | R], Lista):-
	sol1_aux(R, [C], [], ListaInvertida),
	reversa(ListaInvertida, Lista).

% Segunda soluci�n propuesta:
