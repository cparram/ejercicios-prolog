% UN MUNDO DIVIDIDO

amigos(estados_unidos,francia).
amigos(francia,inglaterra).
amigos(union_sovietica,cuba).
amigos(estados_unidos,canada).
amigos(union_sovietica,vietnam).
amigos(chile,estados_unidos).
amigos(alemania_federal,estados_unidos).
amigos(alemania_democratica,union_sovietica).
amigos(union_sovietica,afganistan).
amigos(union_sovietica,corea_del_norte).

belicoso(estados_unidos).
belicoso(francia).
belicoso(inglaterra).
belicoso(union_sovietica).
belicoso(vietnam).
belicoso(alemania_federal).
belicoso(alemania_democratica).
belicoso(corea_del_norte).
belicoso(afganistan).

armamento(estados_unidos,nuclear).
armamento(francia,nuclear).
armamento(canada,convencional).
armamento(inglaterra,nuclear).
armamento(alemania_federal,convencional).
armamento(alemania_democratica,convencional).
armamento(chile,convencional).
armamento(union_sovietica,nuclear).
armamento(corea_del_norte,nuclear).
armamento(vietnam,convencional).
armamento(cuba,convencional).
armamento(afganistan,convencional).

soldados(estados_unidos,20000).
soldados(francia,14000).
soldados(canada,3000).
soldados(inglaterra,16000).
soldados(alemania_federal,13000).
soldados(alemania_democratica,9000).
soldados(chile,6000).
soldados(union_sovietica,25000).
soldados(corea_del_norte,12000).
soldados(vietnam,12000).
soldados(cuba,2000).
soldados(afganistan,7000).
sonAmigos(X, Y) :- amigos(X, Y); amigos(Y, X).

% tu amigo tambie~n es mi amigo
amistad(X, Y) :-  sonAmigos(X, Z), sonAmigos(Z, Y), X  \== Y.

% 1. definicio~n del procedimiento guerra
guerra(X, Y) :- belicoso(X), belicoso(Y), not(amistad(X, Y)).

% 2. definici~on de niveles de alerta para enemigos
% para enemigos de estados_unidos
alertabaja(X) :-
	not(belicoso(X)), not(amistad(estados_unidos, X)), !;
	soldados(X,Y), Y < 5000, not(amistad(estados_unidos, X)).

alertamedia(X) :-
	soldados(X,Y),
	5000 < Y, Y < 15000,
	not(amistad(estados_unidos, X)).

alertagrave(X) :-
	armamento(X,nuclear), not(amistad(estados_unidos, X)), !;
	soldados(X,Y), 15000 < Y, not(amistad(estados_unidos, X)).

% 3. definicio~n de niveles de valor para los pai~ses aliados
% para aliados a estados_unidos

aliadomenor(X) :-
	not(belicoso(X)), amistad(estados_unidos, X), !;
	soldados(X, Y), Y < 5000, amistad(estados_unidos, X).

aliadomedio(X) :-
	soldados(X,Y),
	5000 < Y, Y < 15000,
	amistad(estados_unidos, X).

aliadomayor(X) :-
	armamento(X,nuclear), amistad(estados_unidos, X), !;
	soldados(X,Y), 15000 < Y, amistad(estados_unidos, X).

% DETECTAR AL ESPI~A

% miembro(Nombre, Pai~s, Horas, Edad, Estudios, Viajes)

miembro(1, 'Vannevar Bush', estados_unidos, 9, 55, 2, 4).
miembro(2, 'Klaus Fuchs', alemania, 10, 34, 4, 4).
miembro(3, 'Rudolf Peierls', alemania, 7, 38, 5, 5).
miembro(4, 'Niels Bohr', dinamarca, 12, 60, 1, 4).
miembro(5, 'Alfred L. Loomis', estados_unidos, 5, 58, 2, 6).

% si proviene del extranjero
caso_1(P, Suma):- 
	P \== estados_unidos, Suma is 1, !;
	Suma is 0.

% si pasa ma~s de ocho horas al di~a trabajando
caso_2(Hr, Suma) :-
	Hr > 8, Suma is 1, !;
	Suma is 0.

% si es menor de cuarenta an~os
caso_3(Ed, Suma) :- 
	Ed < 40, Suma is 1, !;
	Suma is 0.

% si tiene ma~s de dos ti~tulos universitarios
caso_4(Es, Suma) :- 
	Es > 2, Suma is 1, !;
	Suma is 0.

% si ha realizado ma~s de tres viajes al extranjero
caso_5(V, Suma) :- 
	V > 3, Suma is 1, !;
	Suma is 0.

indice_sospecha(P, Hr, Ed, Es, V, Sospecha) :-
	
	caso_1(P, Sum1),
	caso_2(Hr, Sum2),
	caso_3(Ed, Sum3),
	caso_4(Es, Sum4),
	caso_5(V, Sum5),	
	Sospecha is Sum1 + Sum2 + Sum3 + Sum4 + Sum5.

miembros(6, _, _, Nombre, Nombre).

miembros(Inicial, IndiceAnterior, IndiceMax, Nombre, Resultado) :-
	miembro(Inicial, N, P, Hr, Ed, Es, V),
	indice_sospecha(P, Hr, Ed, Es, V, Indice),
	Siguiente is Inicial + 1,
	Indice > IndiceAnterior,
	miembros(Siguiente, Indice, Indice, N, Resultado), !;
	miembros(Siguiente, IndiceAnterior, IndiceMax, Nombre, Resultado).

icspy(X) :- 
	miembros(1, 0, 0, _, X).

