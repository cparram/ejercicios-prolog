persona('Juan', 20).
persona('Luis', 23).
persona('Carlos', 42).
deportista('Juan', skateboarding).
deportista('Carlos', snowboarding).

% muestra a las 3 opciones de personas que hay
personas1(X, Y) :-
	persona(X, Y).

personas2(X, Y) :-
	persona(X, Y), 
	deportista(X, _).

% muestra todas las personas, luego las personas deportistas	
personas3(X, Y) :-
	persona(X, Y); 
	deportista(X, _).

% muestra solo una persona y termina
personas4(X, Y) :-
	persona(X, Y), !; 
	deportista(X, _).

% muestra primero al deportista y luego a todas las personas
personas5(X, Y):-
	deportista(X, skateboarding);
	persona(X, Y).

% muestra solo al deportista	
personas6(X, Y):-
	deportista(X, skateboarding), !;
	persona(X, Y).

% muestra a todas las personas
personas7(X, Y):-
	deportista(X, tenis);
	persona(X, Y).

% muestra a todas las personas
personas8(X, Y):-
	deportista(X, tenis), !;
	persona(X, Y).

% false
personas9(X, Y):-
	!, deportista(X, tenis);
	persona(X, Y).
